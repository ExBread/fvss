@echo off

set /P Commit_Text="Write commit text:"
echo %Commit_Text%
git add .
git commit -m "%Commit_Text%"

git fetch -v --progress "origin"
git merge master
git push --progress "origin" master:master

pause>nul