@echo off

set /P Comm="Write a comment: "
echo %Comm%
git add .
git commit -m "%Comm%"

git fetch -v --progress "origin"
git merge master
git push --progress "origin" master:master

pause>nul