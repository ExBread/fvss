@echo off

set /P Commit_description="Commit text:"
echo %Commit_description%
git add .
git commit -m "%Commit_description%"

git fetch -v --progress "origin"
git merge master
git push --progress "origin" master:master

pause>nul